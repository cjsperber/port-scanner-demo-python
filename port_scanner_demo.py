#!/usr/bin/env python

# program description: port scanner using 3 methods: 
# python threads, 3rd party lib gevent, and synchronous calls

# written by c.j. sperber on 1/31/2015

import socket
import threading
import time
from threading import Thread, Semaphore

import gevent
from gevent import socket as gevent_socket


lock = Semaphore(value=1)

def conn_scan_using_threads(host, port):
    try:
        conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        conn.connect((host, port))
        lock.acquire()
        print '[+]%d/tcp open' % port
    except:
        pass
        #print '[-]%d/tcp closed' % port
    finally:
        lock.release()
        conn.close()

def thread_scan(host, high_port):
    start_time = time.time()
    
    print "starting python thread-based scanning at %s (ports 1-%d)" % (host, high_port)

    threads = []
    for port in xrange(1, high_port):
        t = Thread(target=conn_scan_using_threads, args=(host, port))
        t.start()
        #print threading.active_count()
        threads.append(t)
    
    for thread in threads:
        thread.join()

    print "using threading, time in seconds: %0.5f\n" % (time.time() - start_time)


def conn_scan_using_sync(host, port):
    try:
        conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        conn.connect((host, port))
        print '[+]%d/tcp open' % port
        conn.close()
    except:
        pass
        #print '[-]%d/tcp closed' % port

def sync_scan(host, high_port):
    start_time = time.time()

    print "starting python synchronous-based scanning at %s (ports 1-%d)" % (host, high_port)

    for i in range(1, high_port):
        conn_scan_using_sync(host, i)

    print "using sync, time in seconds: %0.5f\n" % (time.time() - start_time)


def conn_scan_using_gevent(host, port):
    try:
        # gevent_socket = gevent.socket; see imports
        socket = gevent_socket.create_connection((host, port))

        print '[+]%d/tcp open' % port
        socket.close()
    except:
        pass
        #print '[-]%d/tcp closed' % port

def gevent_scan(host, high_port):
    start_time = time.time()

    print "starting gevent-based scanning at %s (ports 1-%d)" % (host, high_port)

    threads = [gevent.spawn(conn_scan_using_gevent, host, i) for i in xrange(1, high_port)]
    gevent.joinall(threads)

    print "using gevent, time in seconds: %0.5f\n" % (time.time() - start_time)


# entry point:

print "Program description: testing methods of port scanning using python\n"

HOST = '127.0.0.1'

# port scanning; from ports 1-HIGH_PORT
# any value higher than 1000 will require more advanced
# functionality/constructs (e.g. a threading pool) to handle
# additional thread count; this is only a demonstration
HIGH_PORT = 100

# set socket connection timeout to 1 second
socket.setdefaulttimeout(1)

# synchronous socket functionality is a slow process;
# uncommenting the following line will significantly delay program
#sync_scan(HOST, HIGH_PORT)

thread_scan(HOST, HIGH_PORT)

gevent_scan(HOST, HIGH_PORT)
